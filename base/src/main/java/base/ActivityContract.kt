package base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import extension.*

interface ActivityContract {

    val activity get() = this as AppCompatActivity

    val container: Int

    fun bundleRouter(fragID: Int, bundle: Bundle? = null) {}

    fun onBackPress() = activity.onBackPressed()

    fun popBackStack() = activity.supportFragmentManager.popBackStack()

    fun replaceFragment(fragment: Fragment, stackAdd: Boolean = true) {
        activity.run {
            supportFragmentManager.ensureTransaction().run {
                if (stackAdd) addToBackStack(fragment.javaClass.name)
                supportFragmentManager.setFragmentsVisibleHint(fragment, container)
                replace(container, fragment, fragment.javaClass.name).commit()
            }
        }
    }
}