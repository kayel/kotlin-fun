package base

import android.arch.lifecycle.ViewModel

interface IViewModel<Model : ViewModel> {
    val viewModel : Model
}