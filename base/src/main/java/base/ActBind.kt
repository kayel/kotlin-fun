package base

import android.os.Bundle
import android.view.LayoutInflater
import androidx.viewbinding.ViewBinding
import kotlin.reflect.KClass

abstract class ActBind<T : ViewBinding>(private val bindClass: KClass<T>) : ActBase() {

    lateinit var binding: T

    @Suppress("UNCHECKED_CAST")
    fun inflate() =
        bindClass.java.getMethod("inflate", LayoutInflater::class.java)
            .invoke(null, layoutInflater) as T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = inflate()
        setContentView(binding.root)
        binding.onBoundView()
    }

    abstract fun T.onBoundView()
}