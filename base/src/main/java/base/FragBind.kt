package base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import extension.inflate
import kotlin.reflect.KClass

abstract class FragBind<T : ViewBinding> : FragBase() {

    lateinit var binding: T
    abstract val bindClass: KClass<T>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, state: Bundle?): View {
        binding = inflate(bindClass)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.onBoundView()
    }

    abstract fun T.onBoundView()

}
