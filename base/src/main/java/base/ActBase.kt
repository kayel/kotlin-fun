package base

import android.arch.lifecycle.ViewModel
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinfun.base.R

open class ActBase(private val layout: Int = R.layout.act_frame) : AppCompatActivity() {

    open val viewModel: ViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout)
        intent?.extras?.onExtras()
        viewModel?.onViewModel()
        onView()
    }

    open fun Bundle.onExtras() {}

    open fun ViewModel.onViewModel() {}

    open fun onView() {}
}
