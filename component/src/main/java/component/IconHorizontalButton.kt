package component

import android.content.Context
import android.support.annotation.VisibleForTesting
import android.util.AttributeSet
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import kotlinfun.component.R

class IconHorizontalButton : FrameLayout {

    @VisibleForTesting
    lateinit var iconView: TextView

    @VisibleForTesting
    lateinit var textView: TextView

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    override fun getAccessibilityClassName(): CharSequence = Button::class.java.name

    fun init(attrs: AttributeSet?) {
        inflate(context, R.layout.view_icon_horizontal_button, this)
        iconView = findViewById(R.id.icon_view)
        textView = findViewById(R.id.text_view)
        attrs?.let {
            val attributes = context.obtainStyledAttributes(it,
                R.styleable.IconHorizontalButton, 0, 0)
            setFontIcon(attributes.getString(R.styleable.IconHorizontalButton_fontIcon))
            setText(attributes.getString(R.styleable.IconHorizontalButton_text))

            attributes.recycle()
        }
    }

    fun getFontIcon(): String? = iconView.text?.toString()

    fun setFontIcon(icon: String?) {
        iconView.text = icon
    }

    fun getText(): String? = textView.text?.toString()

    fun setText(text: String?) {
        textView.text = text
    }
}
