package component

import android.content.Context
import android.support.v4.content.res.ResourcesCompat.getFont
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import android.widget.Button
import extension.viewAs
import kotlinfun.component.R

class FunButtonWithIcon @JvmOverloads constructor(context: Context, attributeSet: AttributeSet? = null, defStyleAttr: Int = 0)
    : BaseFunButton(context, attributeSet, defStyleAttr) {

    init {
        inflate(context, R.layout.view_fun_button_with_icon, this)
        buttonTitle = findViewById<AppCompatTextView>(R.id.fun_button_title)
        buttonTitle.typeface = getFont(context, R.font.fun_text_app_bold)
        attributeSet?.let { setAttributes(it) }
        this viewAs Button::class
    }

    override fun setAttributes(attributeSet: AttributeSet) {
        context.obtainStyledAttributes(attributeSet,
            R.styleable.FunButtonWithIcon, 0,
            R.style.FunButtonWithIcon
        ).run {
            buttonTitle.run {
                text = getString(R.styleable.FunButtonWithIcon_text)
                typeface = getFont(context, getResourceId(
                    R.styleable.FunButtonWithIcon_textFont,
                    R.font.fun_text_app_bold
                ))
            }
            findViewById<AppCompatTextView>(R.id.fun_button_subtitle).run {
                text = getString(R.styleable.FunButtonWithIcon_subText)
                typeface = getFont(context, getResourceId(
                    R.styleable.FunButtonWithIcon_subTextFont,
                    R.font.fun_text_app_regular
                ))
            }
            findViewById<AppCompatTextView>(R.id.fun_button_arrow).text = getString(R.styleable.FunButtonWithIcon_fontIcon)
            isEnabled = getBoolean(R.styleable.FunButtonWithIcon_enabled, true)
            background = getDrawable(R.styleable.FunButtonWithIcon_backgroundRipple)
            recycle()
        }
    }
}
