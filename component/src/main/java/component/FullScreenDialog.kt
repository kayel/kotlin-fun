package component

import android.app.Dialog
import android.content.Context
import android.view.View
import android.view.View.VISIBLE
import android.view.Window.FEATURE_NO_TITLE
import extension.stringAny
import kotlinfun.component.R
import kotlinx.android.synthetic.main.fullscreen_dialog.fullscreen_dialog_content
import kotlinx.android.synthetic.main.fullscreen_dialog.fullscreen_dialog_icon
import kotlinx.android.synthetic.main.fullscreen_dialog.fullscreen_dialog_negative
import kotlinx.android.synthetic.main.fullscreen_dialog.fullscreen_dialog_neutral
import kotlinx.android.synthetic.main.fullscreen_dialog.fullscreen_dialog_positive
import kotlinx.android.synthetic.main.fullscreen_dialog.fullscreen_dialog_subtitle
import kotlinx.android.synthetic.main.fullscreen_dialog.fullscreen_dialog_title

val DEFAULT_STYLE = R.style.Panel
val ICON_STYLE = R.style.funIconGrayMedium96
val TITLE_STYLE = R.style.funTextGrayMedium24Bold
val SUBTITLE_STYLE = R.style.funTextGrayMedium18Bold
val CONTENT_STYLE = R.style.funTextGrayMedium16

class FullScreenDialog(context: Context, theme: Int = DEFAULT_STYLE) : Dialog(context, theme) {

    constructor(context: Context) : this(context, DEFAULT_STYLE)

    init {
        requestWindowFeature(FEATURE_NO_TITLE)
        setContentView(R.layout.fullscreen_dialog)
    }

    fun dialogIcon(textA: Any? = R.string.ic_agencia): FullScreenDialog {
        fullscreen_dialog_icon.run { text = stringAny(textA) }
        return this
    }

    fun dialogTitle(textA: Any?): FullScreenDialog {
        fullscreen_dialog_title.run { text = stringAny(textA) }
        return this
    }

    fun dialogSubTitle(textA: Int): FullScreenDialog {
        fullscreen_dialog_subtitle.run { text = stringAny(textA) }
        return this
    }

    fun dialogContent(textA: Any?): FullScreenDialog {
        fullscreen_dialog_content.run { text = stringAny(textA) }
        return this
    }

    fun dialogTitle(
        vararg texts: Int?,
        styleA: Int? = TITLE_STYLE,
        styleB: Int? = TITLE_STYLE,
        styleC: Int? = TITLE_STYLE
    ) =
        fullscreen_dialog_title.setDialogTextView(texts, styleA, styleB, styleC)

    fun dialogSubTitle(
        vararg texts: Int?,
        styleA: Int? = SUBTITLE_STYLE,
        styleB: Int? = SUBTITLE_STYLE,
        styleC: Int? = SUBTITLE_STYLE
    ) =
        fullscreen_dialog_subtitle.setDialogTextView(texts, styleA, styleB, styleC)

    fun dialogContent(
        vararg texts: Int?,
        styleA: Int? = CONTENT_STYLE,
        styleB: Int? = CONTENT_STYLE,
        styleC: Int? = CONTENT_STYLE
    ) =
        fullscreen_dialog_content.setDialogTextView(texts, styleA, styleB, styleC)

    private fun StylesTextView.setDialogTextView(
        textList: Array<out Int?>,
        styleA: Int?,
        styleB: Int?,
        styleC: Int?
    ): FullScreenDialog {
        textList.forEachIndexed { index, text ->
            when (index) {
                0 -> {
                    text0 = stringAny(text)
                    style0 = styleA ?: CONTENT_STYLE
                }
                1 -> {
                    text1 = stringAny(text)
                    style1 = styleB ?: CONTENT_STYLE
                }
                2 -> {
                    text2 = stringAny(text)
                    style2 = styleC ?: CONTENT_STYLE
                }
            }
            visibility = VISIBLE
        }
        return this@FullScreenDialog
    }

    fun setNegativeAction(negativeText: Any, listener: Any) = fullscreen_dialog_negative.setListener(listener, negativeText)

    fun setNeutralAction(neutralText: Any, listener: Any) = fullscreen_dialog_neutral.setListener(listener, neutralText)

    fun setPositiveAction(positiveText: Any, listener: Any) = fullscreen_dialog_positive.setListener(listener, positiveText)

    @Suppress("UNCHECKED_CAST")
    private fun BaseFunButton.setListener(listener: Any, textArg: Any): FullScreenDialog {
        when (listener) {
            is View.OnClickListener -> setOnClickListener(listener)
            else -> setOnClickListener { (listener as () -> Unit).invoke() }
        }
        text = stringAny(textArg)
        visibility = VISIBLE
        return this@FullScreenDialog
    }
}
