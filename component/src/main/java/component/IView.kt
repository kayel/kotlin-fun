@file:Suppress("UNCHECKED_CAST")

package component

import android.content.Context
import android.view.View

interface IView {
    val asView: View get() = this as View
    val viewContext: Context get() = asView.context
}
