package component

import android.annotation.SuppressLint
import android.content.Context
import android.text.SpannableStringBuilder
import android.util.AttributeSet
import android.widget.TextView
import android.widget.TextView.BufferType.SPANNABLE
import extension.*
import kotlinfun.component.R

@SuppressLint("AppCompatCustomView")
class LabelView @JvmOverloads constructor(context: Context, val set: AttributeSet? = null, attr: Int = 0, style: Int = 0) :
    TextView(context, set, attr, style) {

    var mayUpdate: Boolean = false
        set(value) {
            field = value
            update()
        }

    var label: CharSequence = EMPTY_STRING
        set(value) {
            field = value
            update()
        }

    var value: CharSequence = EMPTY_STRING
        set(value) {
            field = value
            update()
        }

    var subValue: CharSequence = EMPTY_STRING
        set(value) {
            field = value
            update()
        }

    var labelStyle: Int = R.style.funTextGrayMedium14
        set(value) {
            field = value
            update()
        }

    var valueStyle: Int = R.style.funTextGrayDark18Bold
        set(value) {
            field = value
            update()
        }

    var subValueStyle: Int = R.style.funTextGrayDark14
        set(value) {
            field = value
            update()
        }

    init {
        obtainStyledAttributes(set, R.styleable.LabelView, R.style.LabelView) {
            label = getString(R.styleable.LabelView_label) ?: EMPTY_STRING
            value = getString(R.styleable.LabelView_value) ?: EMPTY_STRING
            subValue = getString(R.styleable.LabelView_sub_value) ?: EMPTY_STRING
            labelStyle = getResourceId(R.styleable.LabelView_label_style, R.style.funTextGrayMedium14)
            valueStyle = getResourceId(R.styleable.LabelView_value_style, R.style.funTextGrayDark18Bold)
            subValueStyle = getResourceId(R.styleable.LabelView_sub_value_style, R.style.funTextGrayDark14)
        }
        enforceFocus()
        mayUpdate = true
    }

    private fun update() {
        if (mayUpdate) {
            SpannableStringBuilder().apply {
                append(buildSpannable("$label\n", labelStyle))
                append(buildSpannable(value, valueStyle))
                subValue.let { if (it.isNotEmpty()) append(buildSpannable("\n$it", subValueStyle)) }
                setText(this, SPANNABLE)
            }
        }
    }

    override fun setText(text: CharSequence?, type: BufferType?) {
        if (type == SPANNABLE) {
            super.setText(text, type)
        } else {
            value = text ?: EMPTY_STRING
            if (visibility != VISIBLE) visibility = VISIBLE
        }
    }

    fun setText(label: Any?, value: Any?, addSpaces: Boolean = false) = apply {
        mayUpdate = false
        if (label is Int) id = label
        this.value = stringAny(value)
        this.label = stringAny(label)
        if (addSpaces) {
            addSpacesBetweenChars()
        }
        mayUpdate = true
    }

    fun addSpacesBetweenChars(labelDesc: CharSequence? = label, valueDesc: String? = value.toString()) = apply {
        contentDescription = "$labelDesc\n${valueDesc?.addSpacesBetweenCharacters()}"
    }
}
