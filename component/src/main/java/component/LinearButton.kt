package component

import android.content.Context
import android.util.AttributeSet
import android.widget.Button
import android.widget.LinearLayout

class LinearButton @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : LinearLayout(context, attrs, defStyleAttr) {
    override fun getAccessibilityClassName(): CharSequence = Button::class.java.name
}
