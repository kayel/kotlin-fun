package component

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import extension.EMPTY_STRING
import extension.obtainStyledAttributes
import kotlinfun.component.R

open class PrintLinearLayout @JvmOverloads constructor(
    context: Context,
    val set: AttributeSet? = null,
    styleAttr: Int = 0,
    defStyle: Int = R.style.PrintLinearLayout
) : LinearLayout(context, set, styleAttr, defStyle), PrintableView {

    override var printer: ViewPrinter? = null
    override var printableFileName: String = "KotlinFun "

    val shareView: ShareView? get() = printer as ShareView?

    init {
        obtainStyledAttributes(set, R.styleable.PrintLinearLayout, defStyle) {
            getResourceId(R.styleable.PrintLinearLayout_printer, 0).let { printerID ->
                if (printerID != 0) printer = (parent as View).findViewById<View>(printerID) as ViewPrinter?
            }
            printableFileName = getString(R.styleable.PrintLinearLayout_file_name) ?: EMPTY_STRING
        }
        orientation = VERTICAL
    }
}

