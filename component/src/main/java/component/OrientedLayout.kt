package component

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import extension.isScreenVertical

class OrientedLayout @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        orientation = if (isScreenVertical) VERTICAL else HORIZONTAL
    }
}
