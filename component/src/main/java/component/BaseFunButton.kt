package component

import android.content.Context
import android.util.AttributeSet
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView

abstract class BaseFunButton(context: Context, attributeSet: AttributeSet?, defStyleAttr: Int)
    : FrameLayout(context, attributeSet, defStyleAttr) {

    var text: CharSequence
        get() = buttonTitle.text
        set(value) {
            buttonTitle.text = value
        }

    lateinit var buttonTitle: TextView

    override fun getAccessibilityClassName(): String = Button::class.java.name

    abstract fun setAttributes(attributeSet: AttributeSet)
}
