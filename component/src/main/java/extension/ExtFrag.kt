package extension

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import androidx.viewbinding.ViewBinding
import kotlin.reflect.KClass

fun <T : Fragment> T.newInstance(bundle: Bundle? = null): T {
    arguments = bundle
    return this
}

fun <T : Fragment> T.newInstance(bundleBuilder: Bundle.() -> Unit) = apply {
    Bundle().let {
        bundleBuilder.invoke(it)
        arguments = it
    }
}

fun Fragment.requireAct(block: Fragment.() -> Unit) =
    requireActivity().run { block.invoke(this@requireAct) }

inline fun <reified T : ViewModel> Fragment.viewModel(): T =
    ViewModelProviders.of(this).get(T::class.java)

fun Fragment.isAccessibilityEnabled() = context?.isAccessibilityEnabled()

@Suppress("UNCHECKED_CAST")
fun <T : ViewBinding> Fragment.inflate(kClass: KClass<T>): T =
    kClass.java.getMethod("inflate", LayoutInflater::class.java)
        .invoke(null, layoutInflater) as T

fun <T> Fragment.observe(liveData: LiveData<T>, observe: T.() -> Unit = {}): T? {
    liveData.observe(this, observe)
    return liveData.value
}