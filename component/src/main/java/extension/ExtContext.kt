package extension

import android.accessibilityservice.AccessibilityServiceInfo
import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.support.annotation.StringRes
import android.view.View
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityManager
import java.io.IOException
import kotlin.reflect.KClass

val Context.activity : Activity get() = when (this) {
    is Activity -> this
    else -> (this as ContextWrapper).baseContext.activity
}

fun Context.startActivity(kClass: KClass<*>, extras: Bundle? = null) = Intent(this, kClass.java).let {
    if (extras is Bundle) it.putExtras(extras)
    startActivity(it)
}

fun View.startActivity(kClass: KClass<*>, extras: Bundle? = null) = Intent(context, kClass.java).let {
    if (extras is Bundle) it.putExtras(extras)
    context.startActivity(it)
}

infix fun Context.stringAny(text: Any?): CharSequence = when (text) {
    is String -> text
    is CharSequence -> text
    is Int -> getResourceOrToString(text)
    else -> EMPTY_STRING
}

private fun Context.getResourceOrToString(text: Int) = try {
    getString(text)
} catch (ex: Resources.NotFoundException) {
    text.toString()
}

fun Context?.jsonFile(name: String = "mock/pagamentoDetalhe.json"): String = try {
    this?.assets?.open(name)?.run {
        ByteArray(available()).let {
            read(it)
            close()
            String(it)
        }
    } ?: EMPTY_STRING
} catch (ex: IOException) {
    ex.printStackTrace()
    EMPTY_STRING
}

val Context.accessibilityManager: AccessibilityManager
    get() = getSystemService(Context.ACCESSIBILITY_SERVICE) as AccessibilityManager

fun Context.isAccessibilityEnabled() = accessibilityManager.isEnabled

fun Context.isScreenReaderEnabled(): Boolean {
    if (!accessibilityManager.isEnabled) return false

    val serviceInfoList =
        accessibilityManager.getEnabledAccessibilityServiceList(AccessibilityServiceInfo.FEEDBACK_SPOKEN)
    if (serviceInfoList.isNullOrEmpty()) return false

    return true
}

fun Context.announceAccessibility(@StringRes stringId: Int) = announceAccessibility(getString(stringId))

fun Context.announceAccessibility(stringRes: String) {
    val event = AccessibilityEvent.obtain().apply {
        eventType = AccessibilityEvent.TYPE_ANNOUNCEMENT
        className = javaClass.name
        packageName = packageName
        text.add(stringRes)
    }
    if (isAccessibilityEnabled()) accessibilityManager.sendAccessibilityEvent(event)
}