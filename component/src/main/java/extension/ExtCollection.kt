package extension

fun <T> Collection<T>.get(index: Int): T? {
    forEachIndexed { indexed, element -> if (indexed == index) return element }
    throw IndexOutOfBoundsException()
}