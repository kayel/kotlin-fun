package extension

import android.content.res.Resources
import kotlin.math.roundToInt

fun Number.asDP(toDP: Boolean = true) = (if (toDP)
    (this.toFloat() / Resources.getSystem().displayMetrics.density) else
    (this.toFloat() * Resources.getSystem().displayMetrics.density)
        ).roundToInt()

fun Number.asPX() = asDP(false)