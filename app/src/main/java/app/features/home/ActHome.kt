package app.features.home

import android.os.Bundle
import base.ActivityContract
import base.ActBase
import kotlinfun.app.R
import app.features.home.HomeFragments.FRAG_DETAIL_ID
import app.features.home.HomeFragments.FRAG_HOME_ID
import app.features.home.HomeFragments.FRAG_NOTIFICATIONS_ID
import base.FragBase.Companion.new

object HomeFragments{
    const val FRAG_HOME_ID = 0
    const val FRAG_DETAIL_ID = 1
    const val FRAG_NOTIFICATIONS_ID = 2
}

class ActHome : ActBase(), ActivityContract {

    override val container = R.id.act_frame_container

    override fun onView() {
        replaceFragment(new<FragHome>(Bundle()))
    }

    override fun bundleRouter(fragID: Int, bundle: Bundle?) {
        when(fragID){
            FRAG_HOME_ID -> replaceFragment(new<FragHome>(bundle))
            FRAG_DETAIL_ID -> replaceFragment(new<FragDetail>(bundle))
            FRAG_NOTIFICATIONS_ID -> replaceFragment(new<FragDetail>(bundle))
        }
    }
}