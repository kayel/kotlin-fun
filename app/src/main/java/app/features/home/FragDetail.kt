package app.features.home

import android.os.Bundle
import android.view.View
import android.widget.Button
import extension.onClick
import kotlinfun.app.R
import app.features.home.HomeFragments.FRAG_HOME_ID
import base.FragBase

class FragDetail : FragBase() {

    override val layout: Int
        get() = R.layout.frag_detail

    override fun View.onView() {
        findViewById<Button>(R.id.button).onClick {
            activityContract.bundleRouter(
                FRAG_HOME_ID,
                Bundle().apply {
                    putString("creator", "creator")
                    putString("repoId", "repoId")
                })
        }
    }

    override fun Bundle.onArguments() {
        getString("creator")
        getString("repoId")
    }
}
