package app.features.home

import base.FragBind
import base.IViewModel
import extension.observe
import extension.viewModel
import kotlinfun.app.databinding.FragmentHomeBinding

class HomeFragment : FragBind<FragmentHomeBinding>(),
    IViewModel<HomeViewModel> {

    override val bindClass = FragmentHomeBinding::class
    override val viewModel by lazy { viewModel<HomeViewModel>() }

    override fun FragmentHomeBinding.onBoundView() {
        observe(viewModel.text) { textHome.text = this }
    }
}
