package app.features.home

import android.os.Bundle
import android.view.View
import base.ActBase
import component.newPanel
import extension.find
import extension.onClick
import extension.onClickStart
import kotlinfun.app.R
import app.features.home.HomeFragments.FRAG_DETAIL_ID
import base.FragBase

class FragHome : FragBase() {

    override val layout: Int
        get() = R.layout.frag_home

    override fun View.onView() {
        find(R.id.home_button).onClick {
            activityContract.bundleRouter(
                FRAG_DETAIL_ID,
                Bundle().apply {
                    putString("creator", "creator")
                    putString("repoId", "repoId")
                })
        }

        find(R.id.home_panel).onClick {
            newPanel {

            }
        }

        find(R.id.home_oriented).onClickStart(ActOriented::class)
//
//        find(R.id.home_oriented_recycler).onClickStart(ActOrientedRecycler::class)
//
//        find(R.id.loading_button).onClickStart(ActLoadingOnButton::class)
//
//        find(R.id.scroll_animation).onClickStart(ActScrollAnimation::class)

//        find(R.id.dots_progress_bar).onClickStart(ActDotsProgressBar::class)

        find(R.id.view_material_icon).onClickStart(ActMaterialIcons::class)
    }
}

class ActOriented : ActBase(R.layout.oriented_layout)

//class ActDotsProgressBar : ActBase(R.layout.act_custom_dots_progress_bar)

class ActMaterialIcons : ActBase(R.layout.act_material_icons)