package app.features.home

import android.os.Bundle
import base.ActBase
import extension.bottomNavControl
import kotlinfun.app.R

class ActPager : ActBase(R.layout.act_main) {

    override fun Bundle.onExtras() {

    }

    override fun onView() {
        bottomNavControl(
            R.id.nav_host_fragment,
            R.id.nav_view,
            setOf(
                R.id.navigation_home,
                R.id.navigation_dashboard,
                R.id.navigation_notifications
            )
        )
    }
}
