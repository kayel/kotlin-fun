package mocky.features

import base.ActBind
import component.RecyclerViewBuilder
import extension.get
import extension.onClick
import extension.setup
import extension.toast
import kotlinfun.mocky.databinding.ActOrientedRecyclerBinding
import kotlinfun.mocky.databinding.WhiskasSacheBinding

class ActOrientedRecycler :
    ActBind<ActOrientedRecyclerBinding>(ActOrientedRecyclerBinding::class) {

    override fun ActOrientedRecyclerBinding.onBoundView() {
        orientedRecycler.setup<WhiskasViewBuilder>(listOf(1..999))
    }
}

class WhiskasViewBuilder : RecyclerViewBuilder<Int, WhiskasSacheBinding>() {

    override val bindClass = WhiskasSacheBinding::class.java

    override fun WhiskasSacheBinding.onBind(position: Int) {
        val number = collection.get(position)

        whiskasText.run {
            text = number.toString()
            onClick { toast(number) }
        }
    }
}