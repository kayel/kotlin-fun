package mocky.features

import android.support.v4.widget.NestedScrollView
import base.ActBase
import extension.asPX
import extension.scaleViewOnScroll
import kotlinfun.mocky.R

class ActScrollAnimation : ActBase(R.layout.scroll_animation_example) {

    override fun onView() {
        super.onView()
        val nestedScrollView = findViewById<NestedScrollView>(R.id.nested_scroll)

        nestedScrollView.scaleViewOnScroll(
            view = findViewById(R.id.image_scroll),
            maxHeight = 200.0.asPX(),
            minHeight = 100F.asPX(),
            scrollSize = 100.asPX(),
            shouldFade = true
        )
    }
}
