package mocky.features

import android.os.Bundle
import base.ActBase
import component.ReceiptView
import kotlinx.android.synthetic.main.act_generic_proof_view.*
import kotlinfun.mocky.R

class ActReceipt : ActBase(R.layout.act_generic_proof_view) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configBalanceLoading()
    }

    private fun configBalanceLoading() {
        val btnList = proof_list
        for (i in 0 until btnList.childCount) {
            val childAt = btnList.getChildAt(i)
            if (childAt is ReceiptView)
                childAt.balanceListenner = object : ReceiptView.BalanceInfoListenner {
                    override fun onRequestBalance() {
                        childAt.postDelayed({
                            if (i == 3)
                                childAt.setBalanceError()
                            else
                                childAt.setBalance("R$ 2.540,00")
                        }, 1500)
                    }
                }
        }
    }
}
