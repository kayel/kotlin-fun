package mocky.features

import android.os.Bundle
import android.widget.Toast.LENGTH_SHORT
import android.widget.Toast.makeText
import component.FullScreenDialog
import kotlinx.android.synthetic.main.act_full_screen_dialog.*
import kotlinfun.mocky.R
import base.ActBase

const val POSITIVE = "YES"
const val NEUTRAL = "NEUTRAL"
const val NEGATIVE = "NO"

class ActFullScreenDialog : ActBase(R.layout.act_full_screen_dialog) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        full_screen_dialog_btn.setOnClickListener {
            FullScreenDialog(this)
                    .setNegativeAction(NEGATIVE) { R.string.lorem_ipsum_title.showToast() }
                    .setPositiveAction(POSITIVE) { R.string.lorem_ipsum_subtitle.showToast() }
                    .setNeutralAction(NEUTRAL) { R.string.lorem_ipsum_content.showToast() }
                    .dialogIcon(R.string.ic_agencia)
                    .dialogTitle(R.string.lorem_ipsum_title)
                    .dialogSubTitle(R.string.lorem_ipsum_subtitle)
                    .dialogContent(R.string.lorem_ipsum_content)
                    .show()
        }

        full_screen_dialog_btn2.setOnClickListener {
            FullScreenDialog(this)
                    .setNegativeAction(NEGATIVE) { R.string.lorem_ipsum_title.showToast() }
                    .setPositiveAction(POSITIVE) { R.string.lorem_ipsum_subtitle.showToast() }
                    .dialogIcon(R.string.ic_agencia)
                    .dialogTitle(R.string.lorem_ipsum_title)
                    .dialogSubTitle(R.string.lorem_ipsum_subtitle)
                    .dialogContent(R.string.lorem_ipsum_content)
                    .show()
        }
    }

    private fun String.showToast() = makeText(this@ActFullScreenDialog, this, LENGTH_SHORT).show()

    private fun Int.showToast() = makeText(this@ActFullScreenDialog, this, LENGTH_SHORT).show()
}
